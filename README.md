# DogFinder Fetch FrontEnd Take-Home

This App is to match a possible new owner with a database of sheltered dogs.
Users should add name and email to login in to the search features where they can search by different filters an specific dog.

## Prerequisites

Before start, make sure you have the following software installed:
- [Node.js](https://nodejs.org/) (which includes npm)
- A code editor (e.g., [Visual Studio Code](https://code.visualstudio.com/))

## Installing

Follow these steps to install the project:

1. Clone the repository to your local machine:
   `git clone https://gitlab.com/riqrdo/dogfinder.git`

2. Go to the folder where the project was downloaded or cloned:
   `cd /folder`

3. Intall dependencies
  `npm install`

## Run application

To run the application locally, use the following command:
  `npm start`

The app will start running in development mode and automatically open in your default web browser at http://localhost:3000.

## Test application

To run the test for the application, use the following command:
  `npm test`

Launches the described use cases on the test folder.
Go to [JEST](https://jestjs.io/) to view more about the test framework.

## Technlogies used

- React
- TS
- React Router
- Fetch API
- BEM
- Sass
- React Icons


## Visiting Live Project

This project is deployed into vercel.

[Visit the live project here!](https://dogfinder-psi.vercel.app/)

To now more about Vercel, [click here.](https://vercel.com/)
