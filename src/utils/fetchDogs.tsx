interface fetchRequestProps extends RequestInit{
  method?: 'GET' | 'POST';
  credentials: RequestCredentials;
  headers?: {};
  body?: any
}

const fetchDogs = async (
  queryParams: string,
  requestType: 'GET_DOGS' | 'SEARCH_DOGS' | 'GET_BREEDS' | 'NEXT-PREV_DOGS'  | 'MATCH-DOG',
  requestBody?: any,
): Promise<any> => {
  const requestOptions: fetchRequestProps = {
    credentials: 'include' as RequestCredentials,
  };

  let queryString = '';
  let apiEndpoint = '';

  if(requestType === 'SEARCH_DOGS'){
    apiEndpoint = '/dogs/search';
    requestOptions.method = 'GET';
    queryString = `${queryParams}`;
  }
  else if (requestType === 'GET_DOGS') {
    apiEndpoint = '/dogs';
    requestOptions.method = 'POST';
    requestOptions.headers = {
      'Content-Type': 'application/json',
    };
    requestOptions.body = JSON.stringify(requestBody);
  }
  else if (requestType === 'GET_BREEDS') {
    apiEndpoint = '/dogs/breeds';
    requestOptions.method = 'GET';
  }
  else if (requestType === 'NEXT-PREV_DOGS') {
    apiEndpoint = queryParams;
    requestOptions.method = 'GET';
  }
  else if (requestType === 'MATCH-DOG') {
    apiEndpoint = queryParams;
    requestOptions.method = 'POST';
    requestOptions.headers = {
      'Content-Type': 'application/json',
    };
    requestOptions.body = JSON.stringify(requestBody);
  }

  const apiUrl = `${process.env.REACT_APP_API_URL}${apiEndpoint}`;
  const url = `${apiUrl}${queryString}`;
  try {
    const response = await fetch(url, requestOptions);
    if (!response.ok) {
      return {err: true, msg: response.status.toString()};
    }
    const data = await response.json();
    return data;
  } catch (error) {
    return(error);
  }
};

export default fetchDogs;