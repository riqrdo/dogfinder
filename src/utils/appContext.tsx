
import React from 'react';

export type AppContextType = {
    user : {
        name: string;
        email: string;
    } | null,
    favoriteDogs: string[];
    setUser: React.Dispatch<React.SetStateAction<AppContextType['user']>>;
    setFavoriteDogs: React.Dispatch<React.SetStateAction<string[]>>;
    matchedDog: string[];
    setMatchedDog: React.Dispatch<React.SetStateAction<string[]>>;
    loadingAction: boolean;
    setLoadingAction: React.Dispatch<React.SetStateAction<boolean>>;
  };

const AppContext = React.createContext<AppContextType>({
    user: null,
    setUser: ()=>{},
    favoriteDogs: [],
    setFavoriteDogs: ()=>{},
    matchedDog: [],
    setMatchedDog: ()=>{},
    loadingAction: false,
    setLoadingAction: ()=>{}
});

export default AppContext;