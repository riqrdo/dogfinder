import { useState, ChangeEvent, FormEvent, useContext, useEffect } from 'react';
import Button from '../../components/Button';
import Input from '../../components/Input';
import logo from '../../assets/img/logo.svg';
import AppContext from '../../utils/appContext';
import './_login.scss'

interface FormData {
  name: string;
  email: string;
}

const apiUrl = `${process.env.REACT_APP_API_URL}/auth/login`;

const Login = () => {

  const [formData, setFormData] = useState<FormData>({
    name: '',
    email: '',
  });
  const [error, setError] = useState<boolean>(false);
  const { setLoadingAction, setUser } = useContext(AppContext);
  
  const handleChange = (e: ChangeEvent<HTMLInputElement>) => {
    const { name, value } = e.target;
    setFormData({ ...formData, [name]: value });
  };

  const handleLogin = async (e: FormEvent) => {
    e.preventDefault();
    setLoadingAction(true);
    try {
      const response = await fetch(apiUrl, {
        method: 'POST',
        credentials: 'include',
        headers: {
          'Content-Type': 'application/json',
        },
        body: JSON.stringify(formData),
      });
      if (response.ok) {
        // handleStatus();
        if(formData) setUser({ name: formData.name, email: formData.email});
        
      } else {
        setError(true);
        setLoadingAction(false);
      }
    } catch (error) {
      setError(true);
      setLoadingAction(false);
    }
  };

  useEffect(() => {
    setLoadingAction(false);
  // eslint-disable-next-line react-hooks/exhaustive-deps
  },[])

  return (
    <section className='login'>
      <div className='login__content'>
        <img src={logo} alt='DogFinder' />
        <h1>DogFinder</h1>
        <p>Access to view the dogs looking for a new home!</p>
        <div className='login__divider' />
        <form onSubmit={handleLogin}>
          <Input
            label='Name'
            fieldName='name'
            onChange={handleChange}
            placeholder='Jhon Doe'
            required
            type='text'
            value={formData.name}
            showError={error}
          />
          <Input
            label='Email'
            fieldName='email'
            onChange={handleChange}
            placeholder='your@email.com'
            required
            type='email'
            value={formData.email}
            showError={error}
          />
          <Button type='submit' size='block'>Login</Button>
          {error && 
            <div className='login__error'>
              <strong>Login failed:</strong>
              <span>Please try again later</span>
            </div>
          }
        </form>
      </div>
    </section>
  );
}

export default Login;