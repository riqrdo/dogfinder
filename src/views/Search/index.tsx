import { useEffect, useState, useRef, ChangeEvent, useMemo, useCallback, useContext } from 'react';
import { DogType } from '../../types';
import Button from '../../components/Button';
import Typeahead from '../../components/Typeahead';
import Card from '../../components/Card';
import Pagination from './pagination';
import Input from '../../components/Input';
import Dropdown, { DropdownProps } from '../../components/Dropdown';
import Loader from '../../components/Loader';
import { SortProps, OrderProps  } from '../../types';
import { FaFilter } from 'react-icons/fa6';
import fetchDogs from '../../utils/fetchDogs';
import AppContext from '../../utils/appContext';
import './_search.scss'

const sortOptions:DropdownProps['options'] = [
  { value: 'name' as SortProps['options'], label: 'Name'},
  { value: 'breed' as SortProps['options'], label: 'Breed'}
]
const orderOptions:DropdownProps['options'] = [
  { value: 'asc' as OrderProps['options'], label: 'Ascending'},
  { value: 'desc' as OrderProps['options'], label: 'Descending'}
]

const loadSize = 24;

const Search = () => {
  
  const [dogs, setDogs] = useState<DogType[] | null>(null);
  const [dogsIds, setDogsIds] = useState<string[]>([]);
  const [totalDogs, setTotalDogs] = useState<number>(0);
  const [nextDogs, setNextDogs] = useState<string>('');
  const [prevDogs, setPrevDogs] = useState<string>('');
  const [sorting, setSorting] = useState<SortProps['options']>('breed');
  const [order, setOrder] = useState<OrderProps['options']>('asc');
  const [currentPage, setCurrentPage] = useState<number>(1);
  const [breedsSelected, setBreedsSelected] = useState<string[]>([]);
  const [maxAge, setMaxAge] = useState<string>('');
  const [minAge, setMinAge] = useState<string>('');
  const [breeds,setBreeds] = useState<string[]>([]);
  const [filtersShow, setFiltersShow] = useState<boolean>(false);
  const [loading, setLoading] = useState<boolean>(true);
  const [apiError, setApiError] = useState<{err:boolean; msg: string}>({ err: false, msg: '' });
  const dogsListRef = useRef<HTMLDivElement>(null);
  const { favoriteDogs, setFavoriteDogs, matchedDog, setLoadingAction } = useContext(AppContext);

  const param = useMemo(() => {
    let charBreeds;
    if (breedsSelected.length > 0) {
      charBreeds = breedsSelected.map(str => str.replace(/ /g, '%20'));
      charBreeds = charBreeds.join(',');
    }
    const queryParams = `?
      size=${loadSize}
      &sort=${sorting}:${order}
      &from=0
      ${maxAge !== '' ? `&ageMax=${maxAge}` : ''}
      ${minAge !== '' ? `&ageMin=${minAge}` : ''}
      ${breedsSelected.length > 0 ? `&breeds=${charBreeds}` : ''}
    `.replace(/ /g, '').replace(/\n/g, '');
    return queryParams;
  }, [maxAge, minAge, breedsSelected, sorting, order]);

  const handleSearch = useCallback(() => {
    fetchDogs(param, 'SEARCH_DOGS').then(data => {
      setCurrentPage(1);
      updateDogsData(data);
      setFiltersShow(false);
    });
  // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [param]);

  const handleFavorites = (dogID: string) => {
    const updatedFavoriteDogs = [...favoriteDogs, dogID];
    setFavoriteDogs(updatedFavoriteDogs);
  };

  const handleClearSearch = useCallback(() => {
    setMinAge('');
    setMaxAge('');
    setOrder('asc');
    setSorting('breed');
    setFiltersShow(false);
    setBreedsSelected([]);
    fetchDogs('?size=24&sort=breed:asc&from=0', 'SEARCH_DOGS').then(data => updateDogsData(data));
  // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  const openFilters = () => {
    setFiltersShow(true);
  };

  const updateDogsData = (data: any) => {
    if(data.hasOwnProperty('resultIds')) {
      setDogsIds(data.resultIds);
      setTotalDogs(data.total);
      data.hasOwnProperty('prev') ? setPrevDogs(data.prev) :  setPrevDogs('');
      data.hasOwnProperty('next') ? setNextDogs(data.next) :  setNextDogs('');
      window.scrollTo({
        top: 0,
        behavior: 'smooth',
      });
      setApiError({err: false, msg: ''});
    } else {
      setApiError({err: true, msg: `There's a problem, please try again later${data.msg ? ` : ${data.msg}` : "."}`});
    }
  };

  const getNextOrPrevDogs = (direction: string) => {
    let queryUrl = '';
    if (direction === 'next') {
      queryUrl = nextDogs;
      setCurrentPage(current => current+1);
    }
    if (direction === 'prev') {
      queryUrl = prevDogs;
      setCurrentPage(current => current-1)
    }
    fetchDogs(queryUrl, 'NEXT-PREV_DOGS')
    .then(data => updateDogsData(data));
  };

  const getLastDogs = () => {
    let lastOfDogs = Math.floor(totalDogs / loadSize);
    lastOfDogs = lastOfDogs * loadSize;
    lastOfDogs = lastOfDogs > (10000-loadSize) ? 10000-loadSize : lastOfDogs;
    let lastQuery = nextDogs !== '' ? nextDogs : prevDogs;
    lastQuery = lastQuery.replace(/&from=\d+/,`&from=${lastOfDogs}`);
    setCurrentPage(Math.ceil(totalDogs / loadSize));
    fetchDogs(lastQuery, 'NEXT-PREV_DOGS')
    .then(data => updateDogsData(data));
  };

  const getFirstDogs = () => {
    let firstQuery = nextDogs !== '' ? nextDogs : prevDogs;
    firstQuery = firstQuery.replace(/&from=\d+/,`&from=0`);
    setCurrentPage(1);
    fetchDogs(firstQuery, 'NEXT-PREV_DOGS')
    .then(data => updateDogsData(data));
  };

  useEffect(() => {
    //Initial query
    fetchDogs('?size=24&sort=breed:asc&from=0', 'SEARCH_DOGS')
    .then((data) => updateDogsData(data));
    //Get breeds
    fetchDogs('/dogs/breedsas', 'GET_BREEDS')
    .then((data) => setBreeds(data));

    setLoadingAction(false);
  // eslint-disable-next-line react-hooks/exhaustive-deps
  },[]);

  useEffect(() => {
    //get dogs (query, type, the ID's on dogs results)
    fetchDogs(param,'GET_DOGS',dogsIds)
    .then(data => {
      
      if(data.hasOwnProperty('')) {  
        console.log(data);
        setApiError({err: true, msg: data.msg})
      } else {
        setDogs(data);
      }
      setLoading(false);
    });
  // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [dogsIds]);

  return (
    <section className='dogs-search inner'>
      <div className='content'>
        <aside className={`dogs-search__finder ${filtersShow ? 'dogs-search__finder--active' : ''}`}>
          <div>
            <h3>Filter options</h3>
            {breeds.length > 0 && (
                <Typeahead
                  name='breeds-opts'
                  obj={breeds}
                  placeholder='Type a breed'
                  updateSelections={(opt) => setBreedsSelected(opt)}
                  selections={breedsSelected}
                />
            )}
            <div className='dogs-search__finder-age'>
              <Input
                type='number'
                value={minAge}
                fieldName='minage'
                label='Min Age'
                onChange={(e:ChangeEvent<HTMLInputElement>) => setMinAge(e.target.value.toString())}
                props={
                  { min: '1', max: maxAge }
                }
              />
              <Input
                type='number'
                value={maxAge}
                fieldName='maxage'
                label='Max Age'
                onChange={(e:ChangeEvent<HTMLInputElement>) => setMaxAge(e.target.value.toString())}
                props={
                  { min: minAge, max: '20' }
                }
              />
            </div>
            <Dropdown
              value={sorting}
              options={sortOptions}
              fieldName='sortdogs'
              onChange={(e:ChangeEvent<HTMLSelectElement>) => setSorting(e.target.value as SortProps['options'])}
              label='Sort by'
            />
            <Dropdown
              value={order}
              options={orderOptions}
              fieldName='orderdogs'
              onChange={(e:ChangeEvent<HTMLSelectElement>) => setOrder(e.target.value as OrderProps['options'])}
              label='Order'
            />
            <div className='dogs-search__finder-actions'>
              <Button
                type='button'
                size='block'
                onClick={handleSearch}
                disabled={apiError.err}
              >
                  Apply
              </Button>
              <Button 
                altClass='secondary'
                type='button'
                size='block'
                onClick={handleClearSearch}
              >
                  Clear Search
              </Button>
            </div>
          </div>
        </aside>
        <div className='dogs-search__list' ref={dogsListRef}>
          {loading && <Loader />}
          {dogs && (
              <>
                {dogsIds.length > 0
                  ? dogs.map((dog: DogType) => (
                    <Card
                      cardData={dog}
                      key={dog.id}
                      clickFavorite={handleFavorites}
                      isMatched={matchedDog.includes(dog.id)}
                      isFavorite={favoriteDogs.includes(dog.id)}
                    />
                  ))
                  : <div className='dogs-search__noresults'>
                      <span>We couldn't find any results matching the selected criteria.</span><br /><br />
                      <span>Failed query: <br /> {`/dogs/search/${param}`}</span>
                    </div>
                }
                {(dogs.length > 0 && totalDogs > loadSize) && (
                  <Pagination
                    currentPage={currentPage}
                    totalCount={totalDogs}
                    pageSize={loadSize}
                    onNextPage={() => getNextOrPrevDogs('next')}
                    onPrevPage={() => getNextOrPrevDogs('prev')}
                    onLastPage={() => getLastDogs()}
                    onFirstPage={() => getFirstDogs()}
                  />
                )}
              </>
            ) 
          }
          {(!loading && apiError.err === true) && <div className='dogs-search__noresults'>{apiError.msg}</div>}
        </div>
      </div>
      {!filtersShow && <button className='dogs-search__floating' onClick={openFilters}><FaFilter /></button>}
    </section>
  );
};

export default Search;