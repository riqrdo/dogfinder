import React from 'react';
import { usePagination, DOTS } from '../../hooks/usePagination';
import { FaAngleRight, FaAngleLeft, FaAnglesRight, FaAnglesLeft } from "react-icons/fa6";

interface PaginationProps {
  onPageChange?: (page: number) => void;
  onNextPage: () => void;
  onPrevPage: () => void;
  onLastPage?: () => void;
  onFirstPage?: () => void;
  totalCount: number;
  siblingCount?: number;
  currentPage: number;
  pageSize: number;
}

const Pagination: React.FC<PaginationProps> = (props) => {
  const {
    onPageChange,
    onNextPage,
    onPrevPage,
    onLastPage,
    onFirstPage,
    totalCount,
    siblingCount = 1,
    currentPage,
    pageSize,
  } = props;

  // Asegúrate de que paginationRange esté definido antes de usarlo
  const paginationRange = usePagination({
    currentPage,
    totalCount,
    siblingCount,
    pageSize,
  }) || [];

  if (currentPage === 0 || paginationRange.length < 2) {
    return null;
  }

  const onPageSelection = (page: number) => {
    if(onPageChange){
      onPageChange(page);
    }
  };

  let lastPage = paginationRange[paginationRange.length - 1];
  return (
    <ul className='dogs-search__pagination'>
      <li className='prev initial' data-disabled={currentPage === 1} onClick={onFirstPage}>
        <FaAnglesLeft />
      </li>
      <li className='prev' onClick={onPrevPage} data-disabled={currentPage === 1}>
        <FaAngleLeft />
      </li>
      { onPageChange
        ? 
          paginationRange.map((pageNumber, index) => {
            if (pageNumber === DOTS) {
              return <li key={index} className="dots">&#8230;</li>;
            }
            return (
              <li key={index} className={`item ${currentPage === pageNumber ? "active" : ""}`}>
                <button onClick={() => onPageSelection(pageNumber as number)}>{pageNumber}</button>
              </li>
            )
          })
        :  
        (
          <div className='dogs-search__pagination-info'>
            <div>
              <strong>{currentPage}</strong>
              <em>of</em>
              <strong>{Math.ceil(totalCount / pageSize)}</strong>
            </div>
            <span>Total: {totalCount}</span>
          </div>
        )
      }
      <li className='next' data-disabled={currentPage === lastPage} onClick={onNextPage}>
        <FaAngleRight />
      </li>
      <li className='next final' data-disabled={currentPage === lastPage} onClick={onLastPage}>
        <FaAnglesRight />
      </li>
    </ul>
  );
};

export default Pagination;
