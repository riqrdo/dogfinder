import { render, fireEvent, waitFor, screen, act } from '@testing-library/react';
import Search from '../views/Search';

describe('Search Component', () => {
  it('renders Search component and performs a search', async () => {
    
    global.fetch = jest.fn();
  
    // renders comp
    render(<Search />);
  
    // trigger
    // eslint-disable-next-line testing-library/no-unnecessary-act
    act(() => {
      fireEvent.click(screen.getByText('Apply'));
    });
  
    // wait repsonse
    await waitFor(() => {
      expect(global.fetch).toHaveBeenCalledWith(
        expect.stringContaining('/dogs/search?size=24&sort=breed:asc&from=0'),
        {
          method: 'GET',
          credentials: 'include',
        }
      );
    });
  
  });
  
});
