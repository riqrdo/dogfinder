import { render, fireEvent, waitFor, screen } from '@testing-library/react';
import '@testing-library/jest-dom';
import Login from '../views/Login';

describe('Login Component', () => {

  it('renders Login component', () => {
    const { getByText } = render(<Login />);
    // Check if the component's content is rendered
    expect(screen.getByText('DogFinder')).toBeInTheDocument();
    expect(screen.getByText('Access to view the dogs looking for a new home!')).toBeInTheDocument();
  });

  it('handles Login and API response', async () => {
    // Mock the fetch function to simulate API response
    global.fetch = jest.fn().mockResolvedValue({ ok: true });

    render(<Login />);
        // add data form
    fireEvent.change(screen.getByLabelText('Name'), { target: { value: 'John Doe' } });
    fireEvent.change(screen.getByLabelText('Email'), { target: { value: 'test@example.com' } });
    
    // trigger
    fireEvent.submit(screen.getByText('Login'));

    // async for response
    await waitFor(() => {
      expect(global.fetch).toHaveBeenCalledWith(expect.stringContaining('/auth/login'), {
        method: 'POST',
        credentials: 'include',
        headers: {
          'Content-Type': 'application/json',
        },
        body: JSON.stringify({ name: 'John Doe', email: 'test@example.com' }),
      });
    });
  });
});
