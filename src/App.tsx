import { useState, useEffect } from "react";
import {
  BrowserRouter as Router,
  Route,
  Routes,
  Navigate
} from "react-router-dom";
import Header from "./components/Header";
import Login from "./views/Login";
import Search from "./views/Search";
import AppContext,{AppContextType} from "./utils/appContext";
import { DogType } from './types';
import fetchDogs from './utils/fetchDogs';
import Modal from "./components/Modal";
import Card from "./components/Card";
import Loader from "./components/Loader";

const App = () => {

  const [favDogs, setFavDogs] = useState<AppContextType['favoriteDogs']>([]);
  const [matchedDog, setMatchedDog] = useState<AppContextType['matchedDog']>([]);
  const [loadingAction, setLoadingAction] = useState<AppContextType['loadingAction']>(false);
  const [user, setUser] = useState<AppContextType['user']>(null);
  const [matchedDogModal, setMatchedDogModal] = useState<boolean>(false);
  const [dogToDisplay, setDogToDisplay] = useState<DogType | null>(null);
  const [loadingDog, setLoadingDog] = useState<boolean>(false);

  const closeModal = () => {
    setMatchedDogModal(false);
  };

  const contextValue: AppContextType = {
    user: null,
    setUser: setUser,
    favoriteDogs: favDogs,
    setFavoriteDogs: setFavDogs,
    matchedDog: matchedDog,
    setMatchedDog: setMatchedDog,
    loadingAction: loadingAction,
    setLoadingAction: setLoadingAction
  };

  useEffect(() => {
    if(matchedDog.length > 0){
      setLoadingDog(true);
      
      fetchDogs('/dogs/match','MATCH-DOG',matchedDog)
      .then((data) => {
        setMatchedDogModal(true);
      });

      fetchDogs('','GET_DOGS',matchedDog)
      .then(data => {
        setDogToDisplay(data[0] as DogType);
      })
      .then(() => setLoadingDog(false));
    }
  }, [matchedDog])

  return (
    <AppContext.Provider value={contextValue}>
      <Router>
          {user !== null && <Header />}
          <Routes>
            <Route
              path='/'
              element={
                user !== null
                  ? <Navigate to="/search" replace={true} />
                  : <Login  />
              }
            />
            <Route
              path='/search'
              element={
                user !== null
                  ? <Search />
                  : <Navigate to="/" replace={true} />
                }
            />
          </Routes>
      </Router>
      {
        (loadingDog && dogToDisplay)
          ? <Loader showFull />
          : <Modal show={matchedDogModal} closeModal={closeModal}>
              <h1>Congratulations!</h1>
              <p>You match a dog!</p>
              {dogToDisplay && <Card cardData={dogToDisplay} isMatched />}
            </Modal>
      }
      {loadingAction && <Loader showFull />}
    </AppContext.Provider>
  )
}

export default App;
