export interface DogType {
  id: string;
  img: string;
  name: string;
  age: number;
  zip_code: string;
  breed: string;
}

export interface SortProps {
  options: "breed" | "name";
}

export interface OrderProps {
  options: "asc" | "desc";
}