import './_loader.scss';
import { FaSpinner } from "react-icons/fa6";

interface LoaderProps {
  showFull?: boolean;
}

const Loader: React.FC<LoaderProps> = ({ showFull }) => {
  return (
    <div className={`loader ${showFull ? "loader--full" : ""}`}>
      <FaSpinner />
    </div>
  );
};

export default Loader;
