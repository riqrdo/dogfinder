import React from 'react';
import { DogType } from '../../types';
import Button from '../Button';
import { FaHeart, FaRegHeart } from "react-icons/fa6";
import './_card.scss'

interface CardProps {
  cardData: DogType;
  clickFavorite?: (id:string)=>void;
  isMatched?: boolean;
  isFavorite?: boolean;
}

const Card: React.FC<CardProps> = ({ cardData, clickFavorite, isMatched, isFavorite }) => {
  return (
    <div className={`card ${isMatched ? 'card--matched' : ''} `} data-id={cardData.id}>
      <figure>
        {cardData.img && <img src={cardData.img} alt='Dog Pic' />}
      </figure>
      <h4>{cardData.name}</h4>
      <div><span>Age:</span> <em>{cardData.age}</em></div>
      <div><span>Breed:</span> <em>{cardData.breed}</em></div>
      <div><span>Zip Code:</span> <em>{cardData.zip_code}</em></div>
      <div>
      { (!isMatched && clickFavorite) &&
          <Button
            type='button'
            size='block'
            altClass='secondary'
            onClick={ () => clickFavorite(cardData.id) }
            disabled={isFavorite}
          >
            {
            isFavorite
              ? <><FaHeart /> Favorite</>
              : <><FaRegHeart /> Add to favorites</>
            }
          </Button>
        }
        { isMatched &&
          <Button
            type='button'
            size='block'
            altClass='secondary'
            disabled
          >
            <FaHeart /> Your match
          </Button>
        }
      </div>
    </div>
  );
};

export default Card;