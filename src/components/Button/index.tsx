import React from "react";
import './_button.scss'

interface ButtonProps {
  altClass?: string;
  children?: React.ReactNode;
  disabled?: boolean;
  icon?: string;
  onClick?: () => void;
  size?: "block" | "large" | "small";
  type: "button" | "submit" | "reset";
}

const Button: React.FC<ButtonProps> = ({
  altClass,
  children,
  disabled,
  icon,
  onClick,
  size,
  type,
}) => {
  return (
    <button
      type={type}
      className={`btn ${size} ${altClass || ""}`}
      onClick={onClick}
      disabled={disabled}
    >
      {icon && <i className={icon}></i>}
      {children}
    </button>
  );
};

export default Button;
