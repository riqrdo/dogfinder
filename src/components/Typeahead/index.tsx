  import React, { createRef, useEffect, useRef, useState, ChangeEvent } from 'react';
  import { FaTimes } from "react-icons/fa";
  import './_typeahead.scss';

  interface TypeaheadProps {
    obj: any[];
    name?: string;
    placeholder: string;
    className?: string;
    updateSelections: (selections: string[]) => void;
    selections: string[];
  }

  const Typeahead: React.FC<TypeaheadProps> = ({ obj, name, placeholder, selections, updateSelections }) => {
    const [suggestions, setSuggestions] = useState<any[]>([]);
    const [showSuggestions, setShowSuggestions] = useState(false);
    const [typeaheadValue, setTypeaheadValue] = useState('');

    const listSuggestions = useRef<any[]>(suggestions.map(() => createRef()));

    const changedTypeahead = (e: ChangeEvent<HTMLInputElement>) => {
      setTypeaheadValue(e.target.value);
    };

    const handleSuggestionSelected = (selected: string) => {
      updateSelections([...selections, selected]);
      setShowSuggestions(false);
      setTypeaheadValue('');
    };

    const removeSelection = (selection: string) => {
      const newSelections =  selections.filter((item) => item !== selection);
      updateSelections(newSelections);
    };

    useEffect(() => {
      if (typeaheadValue.length > 0) {
        const filteredSuggestions = obj.filter(objItem => 
          objItem.toLowerCase().includes(typeaheadValue.toLowerCase())
        );
        setSuggestions(filteredSuggestions);
        setShowSuggestions(true);
        if (listSuggestions.current && listSuggestions.current.length !== undefined) {
          listSuggestions.current.forEach((ref: any) => {
            ref.current.className = '';
          });
        }
      } else {
        setShowSuggestions(false);
      }
    }, [obj, typeaheadValue]);

    useEffect(() => {
      updateSelections(selections);
    }, [ updateSelections, selections]);

    return (
      <div className='typeahead'>
        <div className='input'>
          <label>Breed</label>
          <input
            type='text'
            placeholder={placeholder}
            onChange={changedTypeahead}
            value={typeaheadValue}
            name={name}
          />
        </div>
        {
          (typeaheadValue.length >= 1 && showSuggestions) &&
          <ul className='typeahead__suggestions'>
            {
              (suggestions.length > 0 && showSuggestions)
                ? suggestions.map((suggest) => (
                  <li
                    key={suggest}
                    onClick={() => handleSuggestionSelected(suggest)}
                  >
                    {suggest}
                  </li>
                ))
                : <li className='no_results'><em>No breeds match your search criteria.</em></li>
            }
          </ul>
        } 
        {selections.length > 0 &&
          <div className='typeahead__selections'>
            { selections.map( (selection) => (
              <div key={selection} onClick={() => removeSelection(selection)}>
                {selection}
                <FaTimes />
              </div>  
            ))}
          </div>
        }
      </div>
    );
  };

  export default Typeahead;