import React, { ChangeEvent } from "react";
import './_dropdown.scss'

export interface DropdownProps {
  errorMessage?: string;
  label?: string;
  fieldName: string;
  onChange: (e: ChangeEvent<HTMLSelectElement>) => void;
  placeholder?: string;
  required?: boolean;
  showError?: boolean;
  props?: Record<string, any>;
  value?: string | number;
  options: {
    value: string;
    label: string;
  }[];
} 

const Dropdown: React.FC<DropdownProps> = ({
  errorMessage,
  label,
  fieldName,
  onChange,
  placeholder,
  required,
  showError,
  value,
  options,
  props
}) => {

  return (
    <div className={`dropdown ${showError ? "error" : ""}`}>
      {label && <label htmlFor={fieldName}>{label}</label>}
      <select
        name={fieldName}
        placeholder={placeholder}
        onChange={onChange}
        required={required}
        value={value}
        {...props}
      >
        {
          options.map((opt) => (
            <option key={opt.value} value={opt.value}>{opt.label}</option>
          ))
        }
      </select>
      {showError && <span className="input__error">{errorMessage}</span>}
    </div>
  );
};

export default Dropdown;
