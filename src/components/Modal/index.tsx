import React, { useRef } from 'react';
import ReactDOM from 'react-dom';
import { FaXmark } from "react-icons/fa6";
import './_modal.scss'

interface ModalProps {
  show: boolean;
  classStyle?: string;
  hideOverlay?: boolean;
  closeModal: () => void;
  children: React.ReactNode;
}

const Modal = ({
  show,
  classStyle,
  hideOverlay,
  closeModal,
  children,
}: ModalProps) => {
  const elem = useRef<HTMLDivElement>(null);

  if (!show) {
    return null;
  }
  return ReactDOM.createPortal(
    <section className={`modal ${classStyle ? classStyle : ''}`}>
      {!hideOverlay && (
        <div className="modal__overlay" onClick={closeModal}></div>
      )}
      <div className="modal__content" ref={elem}>
        <button className="modal__close" onClick={closeModal}>
          <FaXmark />
        </button>
        <div className="modal__info">{children}</div>
      </div>
    </section>,
    document.body
  );
};

export default Modal;
