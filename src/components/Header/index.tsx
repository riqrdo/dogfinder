import { useContext, useEffect, useState } from 'react';
import { Link } from 'react-router-dom';
import { DogType } from '../../types';
import Button from "../Button";
import logo from '../../assets/img/logo.svg';
import AppContext from "../../utils/appContext";
import fetchDogs from '../../utils/fetchDogs';
import Loader from '../Loader';
import './_header.scss';

const apiUrl = `${process.env.REACT_APP_API_URL}/auth/logout`;

const Header = () => {

  const {favoriteDogs, setFavoriteDogs, setMatchedDog, setLoadingAction, setUser} = useContext(AppContext);
  const [dogsInFavs, setDogsInFavs] = useState<DogType[]>([]);
  const [showFavs, setShowFavs] = useState<boolean>(false);
  const [loadingFavs, setLoadingFavs] = useState<boolean>(false); 

  const handleLogOut = async () => {
    setLoadingAction(true);
    try {
      const response = await fetch(apiUrl, {
        method: 'POST',
        credentials: 'include'
      });
      if (response.ok) {
        setUser(null);
        setFavoriteDogs([]);
        setMatchedDog([]);
      } else {
        console.error('Logout failed');
        setLoadingAction(false);
      }
    } catch (error) {
      console.error('Logout failed', error);
      setLoadingAction(false);
    }
  };

  const openFavs = () => {
    setShowFavs(true);
  };

  const removeFromFavs = (dogID: string) => {
    const newFavs = favoriteDogs.filter((dog) => dog !== dogID);
    setFavoriteDogs(newFavs);
  };

  const handleMatch = (dogID: string) => {
    setMatchedDog(match => [dogID]);
  };

  useEffect(() => {
    setLoadingFavs(true);
    fetchDogs('','GET_DOGS',favoriteDogs)
    .then(data => {
      setDogsInFavs(data);
    }).then(() => setLoadingFavs(false));
  }, [favoriteDogs]);

  return (
    <>
      <header className='header'>
        <div className='inner'>
          <Link to={'/search'} className='logo'>
            <img src={logo} alt='DogFinder' />
            DogFinder
          </Link>
          <div className='header__right'>
            <Button type='button' size='small' altClass='tertiary' onClick={openFavs}>
              Your favorites
            </Button>
            <Button type='button' size='small' onClick={handleLogOut}>
              Log out
            </Button>
          </div>
        </div>
      </header>
      
      <section className={`header__fav ${showFavs ? "header__fav--active" : ""}`}>
        <div className='header__fav-overlay' onClick={() => setShowFavs(false)} />
        <div className='header__fav-content'>
          {(!loadingFavs && dogsInFavs.length !== 0) && <p>Your favorites are empty.</p>}
          {(!loadingFavs && dogsInFavs.length > 0) && dogsInFavs.map(dog => (
            <div key={dog.id} className='header__fav-dog'>
              <figure>
                <img src={dog.img} alt='Dog Pic' />
              </figure>
              <div>
                <strong>{dog.name}</strong>
                <span>{dog.breed}</span>  
                <span>{dog.age} {`year${dog.age >= 1 ? "s" : ""}`}</span>
                <span>{dog.zip_code}</span>
              </div>
              <div className='actions'>
                <Button type='button' size='small' onClick={ () => removeFromFavs(dog.id) }>Remove</Button>
                <Button altClass='secondary' type='button' size='small' onClick={ () => handleMatch(dog.id) }>Match</Button>
              </div>
            </div>
          ))}
          {loadingFavs && <Loader />}  
        </div>
      </section>
    </>
  )
}

export default Header;