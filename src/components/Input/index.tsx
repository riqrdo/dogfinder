import React, { ChangeEvent } from "react";
import './_input.scss'

interface InputProps {
  errorMessage?: string;
  label?: string;
  fieldName: string;
  onChange: (e: ChangeEvent<HTMLInputElement>) => void;
  placeholder?: string;
  required?: boolean;
  showError?: boolean;
  props?: Record<string, any>;
  type: "text" | "password" | "date" | "datetime" | "time" | "number" | "tel" | "email";
  value?: string | number;
} 

const Input: React.FC<InputProps> = ({
  errorMessage,
  label,
  fieldName,
  onChange,
  placeholder,
  required,
  showError,
  type,
  value,
  props
}) => {

  return (
    <div className={`input ${showError ? "error" : ""}`}>
      {label && <label htmlFor={fieldName}>{label}</label>}
      <input
        type={type}
        name={fieldName}
        id={fieldName}
        placeholder={placeholder}
        onChange={onChange}
        required={required}
        value={value}
        {...props}
      />
      {showError && <span className="input__error">{errorMessage}</span>}
    </div>
  );
};

export default Input;
